﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kostka
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void key_r(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 'r')
            {
                Random rnd = new Random();
                int rando = rnd.Next(1, 6);
                textBox1.Text = rando.ToString();
            }
        }

        private void doSchowka(object sender, MouseEventArgs e)
        {
            Clipboard.SetText(textBox1.Text);
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
