﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class dotknij : MonoBehaviour
{
    public GameObject[] action;
    Vector3 objectPos;
    float distance;

    public bool canClick = true;
    public GameObject item;
    public GameObject tempParent;
    bool isClickable = false;
    bool isActive = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        distance = Vector3.Distance(item.transform.position, tempParent.transform.position);
        if (distance > 1f)
        {
            isClickable = false;
        }
        else
        {
            isClickable = true;
        }
        if (Input.GetKeyDown(KeyCode.E) && isActive == false && isClickable == true)
        {
            action[1].SetActive(false);
            action[0].SetActive(true);
            for (int i = 0; i <= 4; i++)
            {
                action[2].transform.position += new Vector3(0, 1, 0);
            }
            isActive = true;
        }

    }
}
