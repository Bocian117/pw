﻿namespace Pesel
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPesel = new System.Windows.Forms.TextBox();
            this.Sprawdz = new System.Windows.Forms.Button();
            this.Alert = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(107, 128);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pesel";
            // 
            // txtPesel
            // 
            this.txtPesel.Location = new System.Drawing.Point(110, 145);
            this.txtPesel.MaxLength = 11;
            this.txtPesel.Name = "txtPesel";
            this.txtPesel.Size = new System.Drawing.Size(145, 20);
            this.txtPesel.TabIndex = 1;
            // 
            // Sprawdz
            // 
            this.Sprawdz.Location = new System.Drawing.Point(110, 172);
            this.Sprawdz.Name = "Sprawdz";
            this.Sprawdz.Size = new System.Drawing.Size(75, 23);
            this.Sprawdz.TabIndex = 2;
            this.Sprawdz.Text = "Sprawdź";
            this.Sprawdz.UseVisualStyleBackColor = true;
            this.Sprawdz.Click += new System.EventHandler(this.Sprawdz_Click);
            // 
            // Alert
            // 
            this.Alert.Location = new System.Drawing.Point(110, 201);
            this.Alert.Multiline = true;
            this.Alert.Name = "Alert";
            this.Alert.ReadOnly = true;
            this.Alert.Size = new System.Drawing.Size(145, 63);
            this.Alert.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 296);
            this.Controls.Add(this.Alert);
            this.Controls.Add(this.Sprawdz);
            this.Controls.Add(this.txtPesel);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPesel;
        private System.Windows.Forms.Button Sprawdz;
        private System.Windows.Forms.TextBox Alert;
    }
}

