﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=komis;";
            string query = "SELECT * FROM pojazdy";

            MySqlConnection databaseConnection = new MySqlConnection(connectionString);
            MySqlCommand commandDatabase = new MySqlCommand(query, databaseConnection);
            commandDatabase.CommandTimeout = 60;
            MySqlDataReader reader;

            try
            {
                databaseConnection.Open();

                reader = commandDatabase.ExecuteReader();

                if (reader.HasRows)
                {
                    MessageBox.Show("Wyświetlam wszystkie pojazdy");
                    while (reader.Read())
                    {

                        string[] row = { reader.GetString(0), reader.GetString(1), reader.GetString(2), reader.GetString(3), reader.GetString(4), reader.GetString(5), reader.GetString(6), reader.GetString(7), reader.GetString(8), reader.GetString(9), reader.GetString(10), reader.GetString(11), reader.GetString(12), reader.GetString(13), reader.GetString(14), reader.GetString(15) };
                        listBox1.Items.Add(row[0]);
                        listBox2.Items.Add(row[1]);
                        listBox3.Items.Add(row[2]);
                        listBox4.Items.Add(row[3]);
                        listBox5.Items.Add(row[4]);
                        listBox6.Items.Add(row[5]);
                        listBox7.Items.Add(row[6]);
                        listBox8.Items.Add(row[7]);
                        listBox9.Items.Add(row[8]);
                        listBox10.Items.Add(row[9]);
                        listBox11.Items.Add(row[10]);
                        listBox12.Items.Add(row[11]);
                        listBox13.Items.Add(row[12]);
                        listBox14.Items.Add(row[13]);
                        listBox15.Items.Add(row[14]);
                        listBox16.Items.Add(row[15]);

                    }
                }
                else
                {
                    MessageBox.Show("Brak rekordów!");
                }
                databaseConnection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void listBox4_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void listBox10_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "" && textBox6.Text != "" && textBox7.Text != "" && textBox8.Text != "" && textBox9.Text != "" && textBox10.Text != "" && textBox11.Text != "" && textBox12.Text != "" && textBox13.Text != "" && textBox14.Text != "" && textBox15.Text != "" && textBox16.Text != "") {

                MySqlConnection con;
                string connectionString = "datasource=127.0.0.1;port=3306;username=root;password=;database=komis;";

                try
                {
                    con = new MySqlConnection();
                    con.ConnectionString = connectionString;
                    con.Open();

                    string query = "INSERT INTO pojazdy (id, model, kolor, rok_prod, nr_vin, przebieg, rodzaj_poj, nr_rej, data_rej, poj_silnika, powypadkowy, dod_wyposarzenie, data_przyjecia, data_wydania, zdjecia, marka)" +
                        " VALUES (@id, @model, @kolor, @rok_prod, @nr_vin, @przebieg, @rodzaj_poj, @nr_rej, @data_rej, @poj_silnika, @powypadkowy, @dod_wyposarzenie, @data_przyjecia, @data_wydania, @zdjecia, @marka)";

                    using (MySqlCommand cmd = new MySqlCommand(query, con))
                    {
                        cmd.Parameters.AddWithValue("@id", null);
                        cmd.Parameters.AddWithValue("@model", textBox2.Text);
                        cmd.Parameters.AddWithValue("@kolor", textBox3.Text);
                        cmd.Parameters.AddWithValue("@rok_prod", textBox4.Text);
                        cmd.Parameters.AddWithValue("@nr_vin", textBox5.Text);
                        cmd.Parameters.AddWithValue("@przebieg", textBox6.Text);
                        cmd.Parameters.AddWithValue("@rodzaj_poj", textBox7.Text);
                        cmd.Parameters.AddWithValue("@nr_rej", textBox8.Text);
                        cmd.Parameters.AddWithValue("@data_rej", textBox9.Text);
                        cmd.Parameters.AddWithValue("@poj_silnika", textBox10.Text);
                        cmd.Parameters.AddWithValue("@powypadkowy", textBox11.Text);
                        cmd.Parameters.AddWithValue("@dod_wyposarzenie", textBox12.Text);
                        cmd.Parameters.AddWithValue("@data_przyjecia", textBox13.Text);
                        cmd.Parameters.AddWithValue("@data_wydania", textBox14.Text);
                        cmd.Parameters.AddWithValue("@zdjecia", textBox15.Text);
                        cmd.Parameters.AddWithValue("@marka", textBox16.Text);

                        cmd.ExecuteNonQuery();
                    }

                    MessageBox.Show("Udało się dodać nowy pojadz do bazy danych");
                }
                catch (Exception a)
                {
                    MessageBox.Show("Error: " + a.Message);
                }

            }else { MessageBox.Show("Wartości nie mogą być puste"); }
        }
    }
}
