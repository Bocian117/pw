﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BMR
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void TextBox1(object sender, EventArgs e)
        {

        }

        private void Reset_Click(object sender, EventArgs e)
        {
            txtMasa.Text = "";
            txtWzrost.Text = "";
            txtWiek.Text = "";
            txtOblicz.Text = "";

            Male.Checked = false;
            Famale.Checked = false;
        }

        private void Oblicz_Click(object sender, EventArgs e)
        {
            if ((txtMasa.Text != "") && (txtWzrost.Text != "") && (txtWiek.Text != ""))
            {
                double Masa = Convert.ToDouble(txtMasa.Text);
                double Wzrost = Convert.ToDouble(txtWzrost.Text);
                double Wiek = Convert.ToDouble(txtWiek.Text);

                Double Oblicz = 0.0;

                if (Male.Checked)
                {
                    Oblicz = (9.99 * Masa) + (6.25 * Wzrost) - (4.92 * Wiek) + 5;
                }
                else if (Famale.Checked)
                {
                    Oblicz = (9.99 * Masa) + (6.25 * Wzrost) - (4.92 * Wiek) - 161;
                }
                txtOblicz.Text = "" + Oblicz;
            }
            else txtOblicz.Text = "Nie podano danych";
        }
    }
}
