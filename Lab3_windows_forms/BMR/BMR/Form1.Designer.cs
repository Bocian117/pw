﻿namespace BMR
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.Reset = new System.Windows.Forms.Button();
            this.Oblicz = new System.Windows.Forms.Button();
            this.txtMasa = new System.Windows.Forms.TextBox();
            this.txtWzrost = new System.Windows.Forms.TextBox();
            this.txtWiek = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOblicz = new System.Windows.Forms.TextBox();
            this.Male = new System.Windows.Forms.RadioButton();
            this.Famale = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // Reset
            // 
            this.Reset.Location = new System.Drawing.Point(62, 286);
            this.Reset.Name = "Reset";
            this.Reset.Size = new System.Drawing.Size(75, 23);
            this.Reset.TabIndex = 0;
            this.Reset.Text = "Reset";
            this.Reset.UseVisualStyleBackColor = true;
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // Oblicz
            // 
            this.Oblicz.Location = new System.Drawing.Point(309, 165);
            this.Oblicz.Name = "Oblicz";
            this.Oblicz.Size = new System.Drawing.Size(75, 23);
            this.Oblicz.TabIndex = 1;
            this.Oblicz.Text = "Oblicz";
            this.Oblicz.UseVisualStyleBackColor = true;
            this.Oblicz.Click += new System.EventHandler(this.Oblicz_Click);
            // 
            // txtMasa
            // 
            this.txtMasa.Location = new System.Drawing.Point(62, 110);
            this.txtMasa.Name = "txtMasa";
            this.txtMasa.Size = new System.Drawing.Size(129, 20);
            this.txtMasa.TabIndex = 2;
            this.txtMasa.TextChanged += new System.EventHandler(this.TextBox1);
            // 
            // txtWzrost
            // 
            this.txtWzrost.Location = new System.Drawing.Point(62, 153);
            this.txtWzrost.Name = "txtWzrost";
            this.txtWzrost.Size = new System.Drawing.Size(129, 20);
            this.txtWzrost.TabIndex = 3;
            // 
            // txtWiek
            // 
            this.txtWiek.Location = new System.Drawing.Point(62, 197);
            this.txtWiek.Name = "txtWiek";
            this.txtWiek.Size = new System.Drawing.Size(129, 20);
            this.txtWiek.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(62, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Masa ciała (kg)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(62, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Wzrost (cm)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Wiek (lata)";
            // 
            // txtOblicz
            // 
            this.txtOblicz.Location = new System.Drawing.Point(294, 194);
            this.txtOblicz.Multiline = true;
            this.txtOblicz.Name = "txtOblicz";
            this.txtOblicz.ReadOnly = true;
            this.txtOblicz.Size = new System.Drawing.Size(100, 64);
            this.txtOblicz.TabIndex = 8;
            // 
            // Male
            // 
            this.Male.AutoSize = true;
            this.Male.Location = new System.Drawing.Point(62, 223);
            this.Male.Name = "Male";
            this.Male.Size = new System.Drawing.Size(78, 17);
            this.Male.TabIndex = 9;
            this.Male.TabStop = true;
            this.Male.Text = "Mężczyzna";
            this.Male.UseVisualStyleBackColor = true;
            // 
            // Famale
            // 
            this.Famale.AutoSize = true;
            this.Famale.Location = new System.Drawing.Point(62, 246);
            this.Famale.Name = "Famale";
            this.Famale.Size = new System.Drawing.Size(61, 17);
            this.Famale.TabIndex = 10;
            this.Famale.TabStop = true;
            this.Famale.Text = "Kobieta";
            this.Famale.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 393);
            this.Controls.Add(this.Famale);
            this.Controls.Add(this.Male);
            this.Controls.Add(this.txtOblicz);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtWiek);
            this.Controls.Add(this.txtWzrost);
            this.Controls.Add(this.txtMasa);
            this.Controls.Add(this.Oblicz);
            this.Controls.Add(this.Reset);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Reset;
        private System.Windows.Forms.Button Oblicz;
        private System.Windows.Forms.TextBox txtMasa;
        private System.Windows.Forms.TextBox txtWzrost;
        private System.Windows.Forms.TextBox txtWiek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtOblicz;
        private System.Windows.Forms.RadioButton Male;
        private System.Windows.Forms.RadioButton Famale;
    }
}

