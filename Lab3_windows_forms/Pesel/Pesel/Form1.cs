﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Pesel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Sprawdz_Click(object sender, EventArgs e)
        {
            string p = txtPesel.Text;
            int d = p.Length;
            if (d != 11) Alert.Text = "Nieprawidłowa długość peselu";
            else
            {
                int wynik = (1*p[0]) + (3*p[1]) + (7*p[2]) + (9*p[3]) + (1*p[4]) + (3*p[5]) + (7*p[6]) + (9*p[7]) + (1*p[8]) + (3*p[9]);
                wynik = wynik % 10;
                Debug.WriteLine(wynik);
                if (p[10] == 10 - wynik) { Alert.Text = "Poprawny pesel"; } else Alert.Text = "Niepoprawny pesel";
            }

        }
    }
}
